﻿namespace PlayGen.Photon.SUGAR
{
    public class Configuration
    {
        public string BaseAddress { get; set; }
     
        public int GameId { get; set; }

        public string AccountName { get; set; }

        public string AccountPassword { get; set; }

        public string AccountSource { get; set; }
    }
}
