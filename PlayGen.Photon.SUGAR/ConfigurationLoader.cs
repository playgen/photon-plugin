﻿using Newtonsoft.Json;
using System.IO;

namespace PlayGen.Photon.SUGAR
{
	public class ConfigurationLoader
	{
		public static Configuration Load()
		{
			//var path = Path.GetDirectoryName(typeof(Configuration).Assembly.Location) + "/SUGARConfiguration.json";
			var path = $"{Directory.GetCurrentDirectory()}/SUGARConfiguration.json";
			var data = File.ReadAllText(path);

			return JsonConvert.DeserializeObject<Configuration>(data);
		}
	}
}
