﻿using System;
using PlayGen.Photon.Analytics.Interfaces;
using PlayGen.SUGAR.Client;
using PlayGen.SUGAR.Common.Shared;
using PlayGen.SUGAR.Contracts.Shared;

namespace PlayGen.Photon.SUGAR
{
	public class AnalyticsServiceAdapter : IDisposable, IAnalyticsService
	{
		private readonly Configuration _sugarConfig;
		private readonly SUGARClient _sugarClient;
		private readonly AccountResponse _loggedInAccount;
		
		private bool _isDisposed;
		private MatchResponse _match;

		public AnalyticsServiceAdapter()
		{
			_sugarConfig = ConfigurationLoader.Load();

			_sugarClient = new SUGARClient(_sugarConfig.BaseAddress);
			_loggedInAccount = _sugarClient.Session.Login(_sugarConfig.GameId, new AccountRequest
			{
				Name = _sugarConfig.AccountName,
				Password = _sugarConfig.AccountPassword,
				SourceToken = _sugarConfig.AccountSource
			});
		}

		~AnalyticsServiceAdapter()
		{
			Dispose();
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			// TODO: dispose is still being called multiple times or something!
			//_sugarClient?.Session.Logout();

			_isDisposed = true;
		}

		public void StartMatch()
		{
			_match = _sugarClient.Match.CreateAndStart();
		}

		public void EndMatch()
		{
			_match = _sugarClient.Match.End(_match.Id);
		}

		public void AddMatchData(string key, int value)
		{
			AddMatchData(key, value, null);
		}

		public void AddMatchRankingData(string category, int rank, int playerSugarId)
		{
			AddMatchData($"RANKING_{category}", rank, playerSugarId);
		}

		private void AddMatchData(string key, long value, int? playerSugarId = null)
		{
			AddMatchData(key, value.ToString(), EvaluationDataType.Long, playerSugarId);
		}		

		private void AddMatchData(string key, string value, EvaluationDataType dataType, int? playerSugarId = null)
		{
			if (playerSugarId == null)
			{
				playerSugarId = _loggedInAccount.User.Id;
			}

			_sugarClient.Match.AddData(new EvaluationDataRequest
			{
				CreatingActorId = playerSugarId,
				EvaluationDataType = dataType,
				GameId = _sugarConfig.GameId,
				Key = key,
				MatchId = _match.Id,
				Value = value
			});
		}
	}
}
