﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlayGen.Photon.Players
{
	public abstract class PlayerManager<TPlayer>
		where TPlayer : Player
	{
		protected readonly Dictionary<int, TPlayer> _players = new Dictionary<int, TPlayer>(10);

		public List<TPlayer> Players => _players.Values.ToList();

		public event Action<int> PlayerLeft;

		public event Action PlayersUpdated;

		public void UpdatePlayer(TPlayer player)
		{
			_players[player.PhotonId] = player;

			OnPlayersUpdated();
		}

		public void ChangeAllState(int state)
		{
			foreach (var player in _players.Values)
			{
				player.State = state;
			}
			OnPlayersUpdated();
		}

		public TPlayer Get(int photonId)
		{
			return _players[photonId];
		}


		public void Remove(int photonId)
		{
			_players.Remove(photonId);
			OnPlayerLeft(photonId);
		}

		public abstract TPlayer Create(int photonId, int? externalId = null, string name = null);

		protected virtual void OnPlayersUpdated()
		{
			PlayersUpdated?.Invoke();
		}

		protected virtual void OnPlayerLeft(int obj)
		{
			PlayerLeft?.Invoke(obj);
		}
	}
}
