﻿using System.Collections;

namespace PlayGen.Photon.Common.Extensions
{
    public static class DictionaryExtensions
    {
        public static TValue ValueOrDefault<TKey, TValue>(this IDictionary dictionary, TKey key)
        {
            TValue value;
            if (dictionary.Contains(key))
            {
                value = (TValue) dictionary[key];
            }
            else
            {
                value = default(TValue);
            }

            return value;
        }
    }
}
