﻿using System.Collections;

namespace PlayGen.Photon.Common.Extensions
{
    public static class HashtableExtensions
    {
        public static TValue ValueOrDefault<TValue>(this Hashtable hashTable, object key)
        {
            TValue value;

            if (hashTable.ContainsKey(key))
            {
                value = (TValue)hashTable[key];
            }
            else
            {
                value = default(TValue);
            }

            return value;
        }
    }
}