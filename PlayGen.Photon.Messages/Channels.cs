﻿namespace PlayGen.Photon.Messages
{
	public enum Channels
	{
		Undefined = 0,

		Room = 1,

		Players = 2,

		Logging = 3,

		Error = 4
	}
}
