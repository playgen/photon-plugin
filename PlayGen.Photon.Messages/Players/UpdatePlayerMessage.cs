﻿using PlayGen.Photon.Players;

namespace PlayGen.Photon.Messages.Players
{
	public class UpdatePlayerMessage<TPlayer> : PlayersMessage
		where TPlayer : Player
	{
		public override int PhotonId => Player.PhotonId;

		public TPlayer Player { get; set; }
	}
}
