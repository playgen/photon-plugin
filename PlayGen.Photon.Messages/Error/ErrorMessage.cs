﻿using PlayGen.Photon.Messaging;

namespace PlayGen.Photon.Messages.Error
{
	public class ErrorMessage : Message
	{
		public override int Channel => (int)Channels.Error;

		public string Message { get; set; }
	}
}
