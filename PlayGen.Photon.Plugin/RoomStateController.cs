﻿using System;
using System.Linq;
using GameWork.Core.States.Event;
using Photon.Hive.Plugin;
using PlayGen.Photon.Players;
using PlayGen.Photon.Plugin.States;

namespace PlayGen.Photon.Plugin
{
	public class RoomStateController<TRoomState, TPlayerManager, TPlayer> : EventStateController<TRoomState>, IDisposable
		where TRoomState : RoomState<TPlayerManager, TPlayer>
		where TPlayerManager : PlayerManager<TPlayer>
		where TPlayer : Player
	{
		private readonly string _startStateName;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="states">The first one will be the initial state</param>
		public RoomStateController(params TRoomState[] states) 
			: base(states)
		{
			_startStateName = states[0].Name;
		}

	    public void OnCreate(ICreateGameCallInfo info)
		{
			// Has to be called on create as photon plugin isn't fully initialized in constructor
			// and will throw a null ref exception if plugin.Broadcast is called.
			Initialize();
			EnterState(_startStateName);

			States[ActiveStateName].OnCreate(info);
		}

		public void OnJoin(IJoinGameCallInfo info)
		{
			States[ActiveStateName].OnJoin(info);
		}

		public void OnLeave(ILeaveGameCallInfo info)
		{
			States[ActiveStateName].OnLeave(info);
		}

		public void Dispose()
		{
			foreach (var state in States.OfType<IDisposable>())
			{
				state.Dispose();
			}
		}
	}
}
