﻿using GameWork.Core.States.Event;
using Photon.Hive.Plugin;
using PlayGen.Photon.Players;

namespace PlayGen.Photon.Plugin.States
{
	public abstract class RoomState<TPlayerManager, TPlayer> : EventState
		where TPlayerManager : PlayerManager<TPlayer>
		where TPlayer : Player
	{
		protected readonly PluginBase PhotonPlugin;
		protected readonly Messenger Messenger;
		protected readonly TPlayerManager PlayerManager;

		protected RoomState(PluginBase photonPlugin, Messenger messenger, TPlayerManager playerManager)
		{
			PhotonPlugin = photonPlugin;
			Messenger = messenger;
			PlayerManager = playerManager;
		}
		
		public virtual void OnRaiseEvent(IRaiseEventCallInfo info)
		{
		}

		public virtual void OnCreate(ICreateGameCallInfo info)
		{
		}

		public virtual void OnJoin(IJoinGameCallInfo info)
		{
		}

		public virtual void OnLeave(ILeaveGameCallInfo info)
		{
		}
	}
}
