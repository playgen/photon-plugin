﻿using System;
using System.Collections;
using ExitGames.Client.Photon.LoadBalancing;
using Photon.Hive.Plugin;
using PlayGen.Photon.Common;
using PlayGen.Photon.Common.Extensions;

namespace PlayGen.Photon.Plugin
{
	public class RoomSettings
	{
		protected readonly PluginBase PhotonPlugin;

		public event Action MinPlayersChangedEvent;

		public bool IsVisible
		{
			get => PhotonPlugin.PluginHost.GameProperties
								.ValueOrDefault<bool>(GamePropertyKey.IsVisible);

			set
			{
				if (IsVisible != value)
				{
					PhotonPlugin.PluginHost.SetProperties(0,
						new Hashtable
							{
							{GamePropertyKey.IsVisible, value}
						},
						null,
						false);
				}
			}
		}

		public bool IsOpen
		{
			get => PhotonPlugin.PluginHost.GameProperties
								.ValueOrDefault<bool>(GamePropertyKey.IsOpen);

			set
			{
				if (IsOpen != value)
				{
					PhotonPlugin.PluginHost.SetProperties(0,
						new Hashtable
							{
							{GamePropertyKey.IsOpen, value}
						},
						null,
						false);
				}
			}
		}

		public int MaxPlayers
		{
			get => PhotonPlugin.PluginHost.GameProperties
								.ValueOrDefault<int>(GamePropertyKey.MaxPlayers);

			set
			{
				if (MaxPlayers != value)
				{
					PhotonPlugin.PluginHost.SetProperties(0,
						new Hashtable
							{
							{GamePropertyKey.MaxPlayers, value}
						},
						null,
						false);
				}
			}
		}

		public int MinPlayers
		{
			get => PhotonPlugin.PluginHost.GameProperties
								.ValueOrDefault<int>(CustomRoomSettingKeys.MinPlayers);
			set
			{
				if (MinPlayers != value)
				{
					PhotonPlugin.PluginHost.GameProperties[CustomRoomSettingKeys.MinPlayers] = value;
					MinPlayersChangedEvent?.Invoke();
				}
			}
		}

		public RoomSettings(PluginBase photonPlugin)
		{
			PhotonPlugin = photonPlugin;
		}
	}
}
