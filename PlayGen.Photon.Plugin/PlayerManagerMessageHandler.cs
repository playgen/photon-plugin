﻿using System;
using System.Linq;
using PlayGen.Photon.Messages;
using PlayGen.Photon.Messages.Players;
using PlayGen.Photon.Messaging;
using PlayGen.Photon.Players;

namespace PlayGen.Photon.Plugin
{
	public class PlayerManagerMessageHandler<TPlayerManager, TPlayer> : IDisposable
		where TPlayerManager : PlayerManager<TPlayer>
		where TPlayer : Player
	{
		private readonly TPlayerManager _playerManager;
		private readonly Messenger _messenger;

		private bool _isDisposed;

		public PlayerManagerMessageHandler(TPlayerManager playerManager, Messenger messenger)
		{
			_playerManager = playerManager;
			_messenger = messenger;

			_messenger.Subscribe((int)Channels.Players, ProcessPlayersMessage);
			_playerManager.PlayersUpdated += BroadcastPlayers;
			_playerManager.PlayerLeft += BroadcastPlayers;
		}

		~PlayerManagerMessageHandler()
		{
			Dispose();
		}

		public void Dispose()
		{
			if (_isDisposed) return;

			_messenger.Unsubscribe((int)Channels.Players, ProcessPlayersMessage);
			_playerManager.PlayersUpdated -= BroadcastPlayers;

			_isDisposed = true;
		}

		private void BroadcastPlayers(int playerId)
		{
			BroadcastPlayers();
		}

		private void BroadcastPlayers()
		{
			_messenger.SendAllMessage(new ListPlayersMessage
			{
				Players = _playerManager.Players.Cast<Player>().ToList()
			});
		}

		public void ProcessPlayersMessage(Message message)
		{
			if (message is UpdatePlayerMessage<TPlayer> updatePlayerMessage)
			{
				_playerManager.UpdatePlayer(updatePlayerMessage.Player);
				return;
			}

			throw new Exception($"Unhandled Players Message: ${message}");
		}
	}
}
