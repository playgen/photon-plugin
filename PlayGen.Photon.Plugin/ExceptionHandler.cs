﻿using System;
using Photon.Hive.Plugin;

namespace PlayGen.Photon.Plugin
{
	public class ExceptionHandler
	{
		private IGamePlugin _photonPlugin;

		public event Action<Exception> ExceptionEvent;

		public Exception Exception { get; private set; }

		internal void SetPlugin(IGamePlugin photonPlugin)
		{
			_photonPlugin = photonPlugin;
		}

		public virtual void OnException(Exception exception)
		{
			Exception = exception;
			ExceptionEvent?.Invoke(exception);
			_photonPlugin.ReportError(1, exception);
		}
	}
}
