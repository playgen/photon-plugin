﻿using System;
using System.Threading;
using NLog;
using Photon.Hive.Plugin;
using PlayGen.Photon.Messages;
using PlayGen.Photon.Messages.Logging;
using PlayGen.Photon.Messaging;
using PlayGen.Photon.Players;
using PlayGen.Photon.Plugin.Interfaces;
using PlayGen.Photon.Messaging.Interfaces;
using PlayGen.Photon.Plugin.Extensions;
using PlayGen.Photon.Plugin.States;

namespace PlayGen.Photon.Plugin
{
	public abstract class RoomControllerPlugin : PluginBase, IDisposable
	{
		public const string PluginName = "RoomControllerPlugin";
		public const int ServerPlayerId = 0;

		private static int InstanceIdCounter;
		protected readonly int InstanceId;
		
		protected RoomControllerPlugin()
		{
			InstanceId = Interlocked.Increment(ref InstanceIdCounter);
		}
		public abstract void Dispose();
	}

	public class RoomControllerPlugin<TRoomStateController, TRoomState, TPlayerManager, TPlayer> : RoomControllerPlugin
		where TRoomStateController : RoomStateController<TRoomState, TPlayerManager, TPlayer>
		where TRoomState : RoomState<TPlayerManager, TPlayer>
		where TPlayerManager : PlayerManager<TPlayer>, new()
		where TPlayer : Player

	{
		private readonly Messenger _messenger;
		private readonly TPlayerManager _playerManager = new TPlayerManager();
		private readonly PlayerManagerMessageHandler<TPlayerManager, TPlayer> _playerManagerMessageHandler;
		private readonly TRoomStateController _stateController;
		private readonly Logger _logger;
		private readonly ExceptionHandler _exceptionHandler;

		private bool _isDisposed;

		public override string Name => PluginName;

		public RoomControllerPlugin(IMessageSerializationHandler messageSerializationHandler, 
			IRoomStateControllerFactory<TRoomStateController, TRoomState, TPlayerManager, TPlayer> roomStateControllerFactory, 
			ExceptionHandler exceptionHandler)
		{
			_exceptionHandler = exceptionHandler;
			_exceptionHandler.SetPlugin(this);


			_logger = LogManager.GetLogger($"{InstanceId}");
			_logger.Info("Started");

			_messenger = new Messenger(messageSerializationHandler, this);
			_stateController = roomStateControllerFactory.Create(this, _messenger, _playerManager, _exceptionHandler);

			_playerManagerMessageHandler = new PlayerManagerMessageHandler<TPlayerManager, TPlayer>(_playerManager, _messenger);

			_messenger.Subscribe((int)Channels.Logging, ProcessLogMessage);
		}

		public override void BeforeCloseGame(IBeforeCloseGameCallInfo info)
		{
			base.BeforeCloseGame(info);
			Dispose();
		}

		~RoomControllerPlugin()
		{
			Dispose();
		}

		public override void Dispose()
		{
			if (_isDisposed) return;

			_messenger.Unsubscribe((int)Channels.Logging, ProcessLogMessage);
			_playerManagerMessageHandler.Dispose();

			_isDisposed = true;
		}

		public override void OnRaiseEvent(IRaiseEventCallInfo info)
		{
			try
			{
				base.OnRaiseEvent(info);

				if (info.Request.EvCode == (byte) EventCode.Message)
				{
					var data = info.Request.Data;
					if (!_messenger.TryProcessMessage((byte[]) data))
					{
						throw new Exception("Couldn't process as message: " + data);
					}
				}
			}
			catch (Exception exception)
			{
				_exceptionHandler.OnException(exception);
			}
		}

		public override void OnCreateGame(ICreateGameCallInfo info)
		{
			try
			{
				base.OnCreateGame(info);

				// First player is always 1 but the player ID isn't initialized by this point.
				var playerId = info.Request.ActorNr > 0
					? info.Request.ActorNr
					: 1;

				_playerManager.Create(playerId);
				_stateController.OnCreate(info);
			}
			catch (Exception exception)
			{
				_exceptionHandler.OnException(exception);
			}
		}

		public override void OnJoin(IJoinGameCallInfo info)
		{
			try
			{
				base.OnJoin(info);

				_playerManager.Create(info.ActorNr);
				_stateController.OnJoin(info);
			}
			catch (Exception exception)
			{
				_exceptionHandler.OnException(exception);
			}
		}

		public override void OnLeave(ILeaveGameCallInfo info)
		{
			try
			{
				_playerManager.Remove(info.ActorNr);
				_stateController.OnLeave(info);

				base.OnLeave(info);
			}
			catch (Exception exception)
			{
				_exceptionHandler.OnException(exception);
			}
		}

		private void ProcessLogMessage(Message message)
		{
			try
			{
				if (message is LogMessage logMessage)
				{
					_logger.Log(logMessage.LogType.ToNLogLevel(),
						$"Client {logMessage.PlayerPhotonId}: {logMessage.Message}");
					return;
				}

				throw new Exception($"Unhandled Log Message: ${message}");
			}
			catch (Exception exception)
			{
				_exceptionHandler.OnException(exception);
			}
		}
	}
}