﻿using System;
using System.Collections.Generic;
using GameWork.Core.Logging;
using PlayGen.Photon.Messages.Logging;

namespace PlayGen.Photon.Plugin.Extensions
{
	public static class LogMessageExtensions
	{
	    private static readonly Dictionary<LogType, NLog.LogLevel> GameWorkNlogMappings =
	        new Dictionary<LogType, NLog.LogLevel>()
	        {
	            {LogType.Verbose, NLog.LogLevel.Trace},
	            {LogType.Debug, NLog.LogLevel.Debug},
	            {LogType.Info, NLog.LogLevel.Info},
	            {LogType.Warning, NLog.LogLevel.Warn},
	            {LogType.Error, NLog.LogLevel.Error},
	            {LogType.Fatal, NLog.LogLevel.Fatal}
            };

		public static NLog.LogLevel ToNLogLevel(this LogType logType) => GameWorkNlogMappings[logType];
	}
}
