﻿using Photon.Hive.Plugin;
using PlayGen.Photon.Players;
using PlayGen.Photon.Plugin.States;

namespace PlayGen.Photon.Plugin.Interfaces
{
	public interface IRoomStateControllerFactory<out TRoomStateController, TRoomState, in TPlayerManager, TPlayer>
		where TRoomStateController : RoomStateController<TRoomState, TPlayerManager, TPlayer>
		where TRoomState : RoomState<TPlayerManager, TPlayer>
		where TPlayerManager : PlayerManager<TPlayer>
		where TPlayer : Player
	{
		TRoomStateController Create(PluginBase photonPlugin, 
			Messenger messenger,
			TPlayerManager playerManager, 
			ExceptionHandler exceptionHandler);
	}
}