﻿using System;
using System.Collections.Generic;

namespace PlayGen.Photon.Messaging
{
	public class MessageSubscriptionHandler
	{
		private readonly Dictionary<int, List<Action<Message>>> _subscribers = new Dictionary<int, List<Action<Message>>>();

		public void Subscribe(int channel, Action<Message> messageRecievedCallback)
		{
			if (!_subscribers.TryGetValue(channel, out var channelSubscribers))
			{
				channelSubscribers = new List<Action<Message>>();
				_subscribers[channel] = channelSubscribers;
			}

			channelSubscribers.Add(messageRecievedCallback);
		}

		public void Unsubscribe(int channel, Action<Message> messageRecievedCallback)
		{
			if (_subscribers.TryGetValue(channel, out var channelSubscribers))
			{
				channelSubscribers.Remove(messageRecievedCallback);
			}
		}


		public void UnsubscribeAll()
		{
			_subscribers.Clear();
		}

		public void SendToSubscribers(Message message)
		{
			if (_subscribers.TryGetValue(message.Channel, out var channelSubscribers))
			{
				foreach (var callback in channelSubscribers.ToArray())
				{
					callback(message);
				}
			}
		}
	}
}
